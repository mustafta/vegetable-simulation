/*
 * Taghreed Safaryan
 *  991494905
 * SYST10199 - Web Programming
 */
package vegetables;

/**
 *
 * @author donmi
 */
public class Beet extends Vegetable{
    
    
   private String color;
    private double size;
    
    
    public Beet(String color, double size) {
        super(color, size);
        this.color = color;
        this.size = size; 
    }
    
    
   @Override
    public String getColor() {
       return color;
    }

    @Override
    public double getSize() {
        return size;
    }
    
      
  
    @Override
    public void isRipe(){
    if (getColor() == "Red" && getSize() == 2)
    {
        System.out.println("Beet is Ripe");
    }
    else{
      System.out.println("Beet is Not Riped");
    }
    
    }
    
}
