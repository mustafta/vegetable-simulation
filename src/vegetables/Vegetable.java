/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package vegetables;
public abstract class Vegetable {
  
    
    private String color;
    private double size;
    
    
    
    public Vegetable(String color, double size)
    
    {
        this.color = color;
        this.size = size;
    
    }

     public abstract String getColor();

     public abstract double getSize();
    
    
    
    public abstract void isRipe();
    
    
}
