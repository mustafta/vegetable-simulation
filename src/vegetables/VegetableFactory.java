
package vegetables;
public class VegetableFactory {
    
    private static VegetableFactory VegFactory;
    private VegetableFactory(){
    
    //get db credentials
    //connect to db
    //keep a handle to the connection 
    
    
    }
    
    public static VegetableFactory getInstance(){
        
        if (VegFactory == null){
        VegFactory = new VegetableFactory();
    }
    return VegFactory;
}
    
    public Vegetable getVegetable(VegetableTypes type, String colour, double size){
    Vegetable veggie =null;
        switch (type)
    {
        case CARROT: 
            veggie =  new Carrot(colour, size);
            break;
        case BEET:
            veggie =  new Beet(colour, size);  
            break;
    
    }
    return veggie ;
    }
}
