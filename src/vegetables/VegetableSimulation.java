
package vegetables;
public class VegetableSimulation {

    public static void main(String[] args) {
        VegetableFactory factory = VegetableFactory.getInstance();
        Vegetable ripeCarrot =  factory.getVegetable(VegetableTypes.CARROT, "Orange ", 3);
        Vegetable ripeBeet =  factory.getVegetable(VegetableTypes.BEET, "Red ", 2.5);
        Carrot anotherCarrot = new Carrot ("Yellow ", 1);
        Beet anotherBeet = new Beet ("Pink ", 1);
        System.out.println(ripeCarrot.getColor() + ripeCarrot.getSize());
        System.out.println(ripeBeet.getColor() + ripeBeet.getSize());
         System.out.println(anotherCarrot.getColor()+anotherCarrot.getSize());
        System.out.println(anotherBeet.getColor()+anotherBeet.getSize());
    }
    
}
