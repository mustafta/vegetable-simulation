/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package vegetables;

/**
 *
 * @author Taghreed
 */
public class Carrot extends Vegetable{

    
    private String color;
    private double size;
    
    
    public Carrot(String color, double size) {
        super(color, size);
        this.color = color;
        this.size = size; 
   }
    
    
   @Override
    public String getColor() {
       return color;
    }

    @Override
    public double getSize() {
        return size;
    }
    
      
  
    @Override
    public void isRipe(){
    if (getColor() == "Orange" && getSize() == 1.5)
    {
        System.out.println("Carrot is Ripe");
    }
    else{
      System.out.println("Carrot is Not Riped");
    }
    
    }

    
}
